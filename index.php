<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<?php 
	include('PathController.php');
	$controller = new PathController; 
	$paths = [
	    '/home/user/folder1/folder2/kdh4kdk8.txt',
		'/home/user/folder1/folder2/565shdhh.txt',
		'/home/user/folder1/folder2/folder3/nhskkuu4.txt',
		'/home/user/folder1/iiskjksd.txt',
		'/home/user/folder1/folder2/folder3/owjekksu.txt'
	];
?>
<h3>Given List path</h3>
	<ul>
	<?php
		foreach ($paths as $path) {
			echo "<li>".$path."</li>";
		}
	?>
	</ul>
<h3>Convert Path to array</h3>
	<?php
		var_dump($controller::pathToNestedArray($paths))
	?>
<h3>Display Path to a Tree Array</h3>
	<pre>
	<?php
		var_dump($controller::pathToNestedArray($paths))
	?>
	</pre>
<h3>Display Path with indention</h3>
	<?php
	$spaceCount = 0;
	$pathArray = $controller::pathToNestedArray($paths);
	$paths = $controller::pathDisplayIndent($pathArray,$spaceCount);
	
	?>
<h3>Generate File paths</h3>
	<pre>
	<?php
	$basePath = "/home/user";
	$path = 5;
	$depth = 3;
	$files = 3;
	$generatedPath = $controller::generatePath($basePath,$path,$depth,$files);
	 var_dump($generatedPath);
	?>
	</pre>
</body>
</html>