<?php

class PathController {

	public static function pathToNestedArray($paths) {

	    $arrayResult = null;
	    foreach ($paths as $path) {
	    	$path = ltrim($path,"/"); //remove first character "/"
	        $parts = explode("/", $path); // convert string to array
	        $arrayResultRef = &$arrayResult;
			foreach ($parts as $part) {
				if (strpos($part, '.txt') !== false) {
				    if(empty($arrayResultRef)){
				    	$arrayResultRef=[];
				    }
				    array_unshift($arrayResultRef,$part);
				} else {
					$arrayResultRef = &$arrayResultRef[$part];
				}
			}
	    }

	    return $arrayResult;
	}

	public static function pathDisplayIndent($paths,$spaceCount){
	    foreach($paths as $key => $value){
	    	//If $value is an array
	        if(is_array($value)){ 
	        	$space = self::addSpaceCounter($spaceCount);
	            echo "<br/>";
	            echo $space . $key;
	            $spaceCount += 4; // add 4 to space
	            self::pathDisplayIndent($value,$spaceCount);
	        } else{
	        	$space = self::addSpaceCounter($spaceCount);
	            echo "<br/>";
	            echo $space . $value;
	        }
	        
	    }
	}

	public static function addSpaceCounter($spaceCount) {
		$space="";
		// Loop counter for space
        	for($i=0;$i<$spaceCount+4;) {
        		$space .= "&nbsp;";
        		$i++;
        	}

    	return $space;
	}

	public static function generatePath($basePath,$path,$depth,$files) {
		$folderName = "folder";
		$folderArray = null;
		for($i=1;$i <= $depth;) {
			$folderArray[] = $folderName .$i;
			$i++;
		}

		$pathArray = [];
		for($i=0;$i<$path;) {
			$folderKey="";
			$folderKeyRand = array_rand($folderArray,1);
			if($folderKeyRand >= 1) {
				for($j=0;$j<=$folderKeyRand;) {
					$folderKey  .= "/" . $folderArray[$j];
					$j++;
				}
			} else {
				$folderKey .= "/" . $folderArray[$folderKeyRand]; 
			}

			$pathResult = $basePath . $folderKey;
			$file = "";
			$file = substr(md5(uniqid(mt_rand(), true)), 0, 8);
			$pathResult .= "/".$file . ".txt"; 
			$pathArray[] =  $pathResult;
			
			$i++;
		}

		return $pathArray;
	}
}

?>